<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Variation;
use App\Attribute;
use CoreComponentRepository;

class VariationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        CoreComponentRepository::instantiateShopRepository();
        $attributes = Attribute::all();
        $variations = Variation::all();
        return view('variation.index', compact('attributes','variations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $variations = Variation::all();
        $attributes = Attribute::latest()->get();
         return view('variation.create', compact('attributes','variations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $variations = new Variation;
        $variations->attribute_id = $request->attribute_id;
        $variations->variation_name = implode('|',$request->variation_name);
        if($variations->save()){
            flash(__('Variation has been inserted successfully'))->success();
            return redirect()->route('variation.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Variation  $variation
     * @return \Illuminate\Http\Response
     */
    public function show(Variation $variation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Variation  $variation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attributes = Attribute::all();
        $variations = Variation::findOrFail(decrypt($id));
        // return $variations;
        return view('variation.edit', compact('attributes','variations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Variation  $variation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $variations = Variation::findOrFail($id);
        $variations->attribute_id = $request->attribute_id;
        $variations->variation_name = implode('|',$request->variation_name);
        if($variations->save()){
            flash(__('Variation has been updated successfully'))->success();
            return redirect()->route('variation.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Variation  $variation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $variation = Variation::findOrFail($id);
        if(Variation::destroy($id)){
            flash(__('Variation has been deleted successfully'))->success();
            return redirect()->route('variation.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }
}
