@extends('layouts.app')

@section('content')

<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Variations Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('variation.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="panel-body">
                <div class="form-group">
                        <div class="col-lg-3">
                        <label class="col-lg-2 control-label">{{__('Attributes')}}</label>
                        </div>
                        <div class="col-lg-9">
                            <select name="attribute_id" id="attribute_id" class="form-control demo-select2" data-placeholder="Choose Attribute">
                                    <option value="">{{__('Select the attribute') }}</option>
                                @foreach($attributes as $key=>$attribute)
                                    <option value="{{ $attribute->id }}" @foreach ($variations as $var) @if($attribute->id == $var->attribute_id) disabled  @endif @endforeach>{{ $attribute->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="variation_name">{{__('Variation Values')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('For Variation Values Type and Hit Enter')}}" id="variation_name" name="variation_name[]" class="form-control" required data-role="tagsinput">
                        <label>[For adding multiple variation values type and hit enter or give comma ","]</label>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>
<script type="text/javascript">
    function getValue(evt) {
        alert('not available');
    }
</script>
@endsection
