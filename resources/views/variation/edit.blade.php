@extends('layouts.app')

@section('content')

<div class="col-lg-6 col-lg-offset-3">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Variation Information')}}</h3>
        </div>

        <!--Horizontal Form-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('variation.update', $variations->id) }}" method="POST" enctype="multipart/form-data">
            <input name="_method" type="hidden" value="PATCH">
            @csrf
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-lg-3">
                        <label class="col-lg-2 control-label">{{__('Variation')}}</label>
                    </div>
                    <div class="col-lg-9">
                        <select class="form-control mb-3 selectpicker" data-placeholder="Select a attribute" id="attribute_id" name="attribute_id">
                            @foreach (\App\Attribute::all() as $attribute)
                            <option value="{{ $attribute->id }}" @if($attribute->id == $variations->attribute_id) selected @else disabled @endif>{{ $attribute->name }}</option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="variation_name">{{__('Variation Values')}}</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="{{__('Type and Hit Enter')}}" id="variation_name" name="variation_name[]" class="form-control" required data-role="tagsinput" value="{{ $variations->variation_name }}">
                        <label>[For variation values type and hit enter or give comma ","]</label>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-purple" type="submit">{{__('Save')}}</button>
            </div>
        </form>
        <!--===================================================-->
        <!--End Horizontal Form-->

    </div>
</div>

@endsection
